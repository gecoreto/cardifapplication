import { Component, OnInit } from '@angular/core';
import { Util } from '../utils/util';
import { IMyDpOptions } from 'mydatepicker';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-style-guide',
  templateUrl: './style-guide.component.html',
  styleUrls: ['./style-guide.component.scss']
})
export class StyleGuideComponent implements OnInit {
  public columnsNum = 1;
  public myDatePickerOptions: IMyDpOptions=Util.getLocaleOptions();//Funcion que establece valores para el datepicker
   //Columnas por dispostivo
   colsMobile = 1; colsTablet = 2; colsPc = 4;
   
     constructor(private commonService:CommonService) {
       this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
       window.onresize = (e) => {
         this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
       };
     }
   
     ngOnInit() {
       this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
     }
     openDialog(id:number){
       var title= id==1?"Error":id==2?"Información":"Exitoso";
       var message= id==1?"Mensaje de Error":id==2?"Mensaje de Información":" Mensaje de Exitoso";
       function provisional(){alert(message)};//La funcion es opcional
       this.commonService.Dialog(id,title,message,provisional);
     }
}
