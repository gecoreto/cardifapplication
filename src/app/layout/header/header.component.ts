import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import {MatMenuModule, MatMenuPanel} from '@angular/material/menu';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  
  @Input() menu_closed;

  constructor() {
    this.menu_closed=true;
   }

  ngOnInit() {
  }

  menuClosed(){
    this.menu_closed=!this.menu_closed;
  }

}
