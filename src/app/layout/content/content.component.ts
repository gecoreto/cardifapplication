import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { SubHeaderComponent } from '../sub-header/sub-header.component';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  providers: [SubHeaderComponent]
})
export class ContentComponent implements OnInit {

  public tit_header :string = "";

  /**
   * Global Variables
   */
  loader: boolean = false;
  header: boolean = true;
  subHeader: boolean = false;
  footer: boolean = true;

  constructor(private commonService: CommonService,private translate: TranslateService) {
    this.commonService.loader$.subscribe(
      loader => {
        this.loader = loader;
      });
  }

  ngOnInit() {
  }

  onSection(component: any) {
    /**
     * Siempre que la variable este definida en el hijo la asigna al atributo de ContentComponent
     * de lo contrario toma el valor por defecto
     */
    this.tit_header= component.tit_header!=undefined ? component.tit_header:"";
    this.header = (component.header != undefined) ? component.header : this.header;
    this.subHeader = (component.subHeader != undefined) ? component.subHeader : this.subHeader;
    this.footer = (component.footer != undefined) ? component.footer : this.footer;
  }

}
