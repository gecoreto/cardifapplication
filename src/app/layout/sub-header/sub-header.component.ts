import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent implements OnInit {

  @Input() tit_header: string;

  constructor() { }

  ngOnInit() {
    console.log("tamaño: ", this.tit_header.length);
    console.log("Titulo: ", this.tit_header);
  }

}
