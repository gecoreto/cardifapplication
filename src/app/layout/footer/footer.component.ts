import { Component, OnInit } from '@angular/core';
import { Util } from '../../utils/util';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  
  columnsNum = 1;
  columnsNumPhones = 2;
  columnsNumEmail = 2;
  listPhones = [
    '(031) 774-4040',
    '(031) 774-4041',
    '(031) 774-4042',
    '(031) 774-4043'
  ];
  listEmails = [
    'info@cardif.com',
    'info@cardif.com',
    'info@cardif.com',
    'info@cardif.com'
  ];
  rowHeightGrid = '80px';
  
  constructor() { 
    window.onresize = (e) => {
      this.columnsNum = this.getNumCols();
    };
  }

  ngOnInit() {
    this.columnsNum = this.getNumCols();
    //Ajuste padding titulos
    var titles = document.getElementsByClassName('title');
    for(let x = 0; x < titles.length; x++){
      var padre = titles[x].parentElement.style.padding = '0';
    }
    //Add scroll phones
    if(this.listPhones.length > 4){
      let phones = document.getElementById('content-phones');
      phones.className = phones.className + ' data-scroll';
    }
    //Add scroll emails
    if(this.listEmails.length > 4){
      let emails = document.getElementById('content-emails');
      emails.className = emails.className + ' data-scroll';
    }
  }

  private getNumCols() {
    var columnNum = 5;
    this.rowHeightGrid = '80px';
    var element = window.innerWidth;
    if (element < 650) {
      columnNum = 1;
      this.rowHeightGrid = '46px';
    }
    return columnNum;
}

}
