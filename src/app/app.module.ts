import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {
  MatGridListModule, MatMenuModule, MatIconModule, MatInputModule,
  MatAutocompleteModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
  MatRadioModule, MatSelectModule, MatListModule, MAT_PLACEHOLDER_GLOBAL_OPTIONS,MatExpansionModule,
  MatTabsModule,MatStepperModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyDatePickerModule } from 'mydatepicker';

//Services
import { CommonService } from './services/common.service';

import { AppComponent } from './app.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { SubHeaderComponent } from './layout/sub-header/sub-header.component';
import { LoaderComponent } from './layout/loader/loader.component';
import { ContentComponent } from './layout/content/content.component';
import { RolesComponent } from './radication/roles/roles.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { StyleGuideComponent } from './style-guide/style-guide.component';
import { QueryClaimsComponent } from './query-claims/query-claims.component';
import { CoveragesComponent } from './radication/coverages/coverages.component';
import { QuestionsComponent } from './radication/questions/questions.component';
import { MessagesComponent } from './utils/dialogs/messages/messages.component';

/*language setting*/
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    SubHeaderComponent,
    LoaderComponent,
    ContentComponent,
    RolesComponent,
    LoginComponent,
    HomeComponent,
    StyleGuideComponent,
    QueryClaimsComponent,
    CoveragesComponent,
    QuestionsComponent,
    MessagesComponent
  ],
  entryComponents: [
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    FlexLayoutModule,
    MatInputModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MyDatePickerModule,
    MatExpansionModule,
    MatTabsModule,
    MatStepperModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [CommonService,
    {provide: MAT_PLACEHOLDER_GLOBAL_OPTIONS, useValue: {float: 'never'}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
