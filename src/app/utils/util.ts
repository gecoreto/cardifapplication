import {IMyDpOptions } from "mydatepicker";

export class Util {
    /*Convierte una fecha tipo Date a un string en formato año/mes/dia: yyyy/mm/dd*/
    static convertDateToStringFormat(date: Date) {
        let yyyy;
        let mm;
        let dd;
        yyyy = date.getFullYear();
        mm = date.getMonth() + 1; /*Enero es 0 por eso se suma 1*/
        dd = date.getDate();

        return yyyy + '/' + (mm < 10 ? '0' + mm : String(mm)) + '/' + (dd < 10 ? '0' + dd : String(dd));
    }

    static setToken(token: string) {
        sessionStorage.setItem('AuthorizationWeb', token);
    }

    static getToken() {
        return sessionStorage.getItem('AuthorizationWeb');
    }

    static sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    static scrollInTop(posY) {
        // var elmnt = document.getElementById("content-scroll");
        // elmnt.scrollTop = 0;
        var body = document.body; // For Safari
        var html = document.documentElement; // Chrome, Firefox, IE and Opera places the overflow at the html level, unless else is specified. Therefore, we use the documentElement property for these browsers
        body.scrollTop += posY;
        html.scrollTop = posY;
        window.scroll(0, posY);
    }
    static getNumCols(colsMobile = 1, colsTablet = 2, colsPc = 3) {
        var columnNum = 3;
        var element = window.innerWidth;
        if (element < 500) {
            columnNum = colsMobile;
        } else if (element > 501 && element < 799) {
            columnNum = colsTablet;
        } else if (element >= 800) {
            columnNum = colsPc;
        }
        return columnNum;
    }
    static getLocaleOptions(): IMyDpOptions {
        /*this.countryCodDesc = window.location.host.split('.').pop().toUpperCase();*/
        var locale = 'es';
        var locales ={
            "en": {
                dayLabels: {su: "Sun", mo: "Mon", tu: "Tue", we: "Wed", th: "Thu", fr: "Fri", sa: "Sat"},
                monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dec" },
                dateFormat: "mm/dd/yyyy",
                showTodayBtn:false,
                firstDayOfWeek: "mo",
                editableDateField:false,
                openSelectorTopOfInput:true,
                showSelectorArrow:false,
                openSelectorOnInputClick:true,
            },
            "es": {
                dayLabels: {su: "D", mo: "L", tu: "M", we: "M", th: "J", fr: "V", sa: "S"},
                monthLabels: {1: "Enero", 2: "Febrero", 3: "Marzo", 4: "Abril", 5: "Mayo", 6: "Junio", 7: "Julio", 8: "Agosto", 9: "Septiembre", 10: "Octubre", 11: "Noviembre", 12: "Diciembre"},
                dateFormat: "dd/mm/yyyy",
                firstDayOfWeek: "mo",
                showTodayBtn:false,
                editableDateField:false,
                openSelectorTopOfInput:true,
                showSelectorArrow:false,
                openSelectorOnInputClick:true,
            },
            "pt-br": {
                dayLabels: {su: "Dom", mo: "Seg", tu: "Ter", we: "Qua", th: "Qui", fr: "Sex", sa: "Sab"},
                monthLabels: { 1: "Jan", 2: "Fev", 3: "Mar", 4: "Abr", 5: "Mai", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Set", 10: "Out", 11: "Nov", 12: "Dez" },
                dateFormat: "dd/mm/yyyy",
                firstDayOfWeek: "mo",
                showTodayBtn:false,
                editableDateField:false,
                openSelectorTopOfInput:true,
                showSelectorArrow:false,
                openSelectorOnInputClick:true,
            }
        };
        if (locale && locales.hasOwnProperty(locale)) {
            // User given locale
            return locales[locale];
        }
        // Default: en
        return locales["en"];
    }
}
