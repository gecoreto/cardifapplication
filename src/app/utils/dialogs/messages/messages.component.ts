import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent{

  public title: string;
  public message: string;
  public typeMessage:number;
  //Funcion que se va ejecutar conado el usuario cierre el mensaje ya sea por boton o con click afuera
  public functionSuccess: Function;
  constructor(public dialogRef: MatDialogRef<MessagesComponent>) { }
}
