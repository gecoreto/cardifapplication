import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesComponent } from '../radication/roles/roles.component';
import { AppComponent } from '../app.component';
import { ContentComponent } from '../layout/content/content.component';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { StyleGuideComponent } from '../style-guide/style-guide.component';
import { QueryClaimsComponent } from '../query-claims/query-claims.component';
import { CoveragesComponent } from '../radication/coverages/coverages.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/claims',
        pathMatch: 'full'
    },
    {
        component: ContentComponent,
        path: 'claims',
        children: [
            {
                path: '',
                component: HomeComponent
            },
            {
                path: 'roles',
                component: RolesComponent
            },
            {
                path: 'menu-query',
                component: HomeComponent
            },
            {
                path: 'coverages',
                component: CoveragesComponent
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        component: ContentComponent,
        path: 'query-claims',
        children: [
            {
                path: '',
                component: QueryClaimsComponent
            }
        ]
    },
    {
        path: 'styleguide',
        component: StyleGuideComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }