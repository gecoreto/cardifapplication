import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  alterContent:boolean = false;
  routerList:any = { option1: '/claims/roles', option2: '/claims/menu-query' };

  constructor(private route: ActivatedRoute) { 
    const url:Observable<string> = route.url.map(segments => segments.join(''));
    url.subscribe(r => {
      if(r == 'menu-query'){
        this.alterContent = true;      
        this.routerList.option1 = '/query-claims/in-progress';
        this.routerList.option2 = '/query-claims/closed';
      }
    });
  }

  ngOnInit() {
  }

}
