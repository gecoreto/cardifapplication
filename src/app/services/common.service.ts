import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MessagesComponent } from '../utils/dialogs/messages/messages.component';

@Injectable()
export class CommonService {

  constructor(private dialog: MatDialog) { }

  // Observable boolean sources
  private loader = new Subject<boolean>();

  // Observable string streams
  loader$ = this.loader.asObservable();

  // Service message commands
  setLoader(data: boolean) {
    this.loader.next(data);
  }
  public Dialog(typeMessage: number, title: string, message: string, functionSuccess?: Function) {
    //funtionSuccess:Funcion que se va ejecutar conado el usuario cierre el mensaje ya sea por boton o con click afuera
    //Si la funcion no es definida no realizara ninguna accion solo cerrar el mensaje.(PopUp o dialog)
    let dialogRef: MatDialogRef<MessagesComponent>;
    dialogRef = this.dialog.open(MessagesComponent);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.typeMessage = typeMessage;
    dialogRef.componentInstance.message = message;
    dialogRef.componentInstance.functionSuccess = functionSuccess;
    if (functionSuccess != undefined) {
      dialogRef.afterClosed().subscribe(result => {
        functionSuccess();
      });
    }
  }
}