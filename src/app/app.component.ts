import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  /*
    se Obtiene lenguaje de la aplicacion segun navegador
    language = window.navigator.language;
  */
  // language setting
  language: string;
  countryId: string;
  countryCodDesc: string;
  countryDesc: string;
  partnerId: string;
  partnerDesc: string;
  channelId: string;
  channelDesc: string;
  appVersion: string;

  constructor(
    private translate: TranslateService,
    private router: Router) {
    this.setDataHeaderService();
    translate.setDefaultLang(this.language);
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  setDataHeaderService() {
    /*this.countryCodDesc = window.location.host.split('.').pop().toUpperCase();*/
    this.countryCodDesc = 'CO';

    switch (this.countryCodDesc) {
      case 'CO':
        this.language = 'es-CO';
        this.countryId = '1';
        this.countryDesc = 'Colombia';
        this.partnerId = '1'; /* Se debe definir como sera cuando ya se en produccion */
        this.partnerDesc = 'Banco Bogotá'; /* Se debe definir como sera cuando ya se en produccion */
        this.channelId = '1'; /* Se debe generar funcion de obtencion de dispositivo spring 4 */
        this.channelDesc = 'web';
        this.appVersion = 'v1';
        break;
      case 'CL':
        this.language = 'es-CL';
        this.countryId = '2';
        this.countryDesc = 'Chile';
        this.partnerId = '0'; /* Se debe definir como sera cuando ya se en produccion */
        this.partnerDesc = 'Cardif'; /* Se debe definir como sera cuando ya se en produccion */
        this.channelId = '1'; /* Se debe generar funcion de obtencion de dispositivo spring 4 */
        this.channelDesc = 'web';
        this.appVersion = 'v1';
        break;
      case 'BR':
        this.language = 'pt-BR';
        this.countryId = '3';
        this.countryDesc = 'Brasil';
        this.partnerId = '0'; /* Se debe definir como sera cuando ya se en produccion */
        this.partnerDesc = 'Cardif'; /* Se debe definir como sera cuando ya se en produccion */
        this.channelId = '1'; /* Se debe generar funcion de obtencion de dispositivo spring 4 */
        this.channelDesc = 'web';
        this.appVersion = 'v1';
        break;
      case 'MX':
        this.language = 'es-CL';
        this.countryId = '4';
        this.countryDesc = 'Mexico';
        this.partnerId = '0'; /* Se debe definir como sera cuando ya se en produccion */
        this.partnerDesc = 'Cardif'; /* Se debe definir como sera cuando ya se en produccion */
        this.channelId = '1'; /* Se debe generar funcion de obtencion de dispositivo spring 4 */
        this.channelDesc = 'web';
        this.appVersion = 'v1';
        break;
      case 'PE':
        this.language = 'es-CL';
        this.countryId = '5';
        this.countryDesc = 'Perú';
        this.partnerId = '0'; /* Se debe definir como sera cuando ya se en produccion */
        this.partnerDesc = 'Cardif'; /* Se debe definir como sera cuando ya se en produccion */
        this.channelId = '1'; /* Se debe generar funcion de obtencion de dispositivo spring 4 */
        this.channelDesc = 'web';
        this.appVersion = 'v1';
        break;
      default:
    }

    const dataHeader = {
      'countryId': this.countryId, 'countryCodDesc': this.countryCodDesc,
      'countryDesc': this.countryDesc, 'partnerId': this.partnerId,
      'partnerDesc': this.partnerDesc, 'channelId': this.channelId,
      'channelDesc': this.channelDesc, 'appVersion': this.appVersion
    };

    sessionStorage.setItem('dataHeader', JSON.stringify(dataHeader));
  }

}
