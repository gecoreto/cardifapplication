import { Component, OnInit, Input } from '@angular/core';
import { Util } from '../../utils/util';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-coverages',
  templateUrl: './coverages.component.html',
  styleUrls: ['./coverages.component.scss']
})
export class CoveragesComponent implements OnInit {

  @Input() columnsNum = 1;
  columnsNumHeader = 3;
  @Input() columnsNumCoverageSelected = 3;
  @Input() coverageSelected = { active: false, index: -1 };
  @Input() coverageIsResponsive = false;
  coverage_to_process = [];
  subHeader=false;
  coverages: any = [
    { name: "cobertura 1", icon: "looks_one" },
    { name: "cobertura 2", icon: "looks_two" },
    { name: "cobertura 3", icon: "looks_3" },
    { name: "cobertura 4", icon: "looks_4" },
  ];
  @Input() questions = [
    {
      "key": "C_//_FINDES",
      "label": "¿En que fecha termino su contrato?",
      "required": true,
      "order": 2,
      "controlType": "date",
      "type": ""
    },
    {
      "key": "C_//_INIDES",
      "label": "¿En que fecha comenzo su contrato?",
      "required": true,
      "order": 2,
      "controlType": "date",
      "type": ""
    },
    {
      "key": "L_//_IU10",
      "label": "¿Cuál fue el motivo de terminacion del contrato?",
      "required": true,
      "order": 3,
      "controlType": "dropdown",
      "options": [
        {
          "key": "IU1001",
          "value": "Despido Masivo "
        },
        {
          "key": "IU1002",
          "value": "Finalizacion del contrato a termino fijo. Sin indemnizacion"
        },
        {
          "key": "IU1003",
          "value": "Despido sin Justa causa con pago de bonificación"
        },
        {
          "key": "IU1004",
          "value": "Mutuo acuerdo con pago de bonificación"
        },
        {
          "key": "IU1005",
          "value": "Contrato suspendido por cualquier causa"
        },
        {
          "key": "IU1006",
          "value": "Decisión unilateral del trabajador (renuncia voluntaria)"
        },
        {
          "key": "IU1007",
          "value": "Despido con justa causa"
        },
        {
          "key": "IU1008",
          "value": "Recorte de personal"
        },
        {
          "key": "IU1009",
          "value": "Restructuración Administrativa y/o Liquidación de la empresa"
        },
        {
          "key": "IU1010",
          "value": "Finalización del periodo de prueba"
        },
        {
          "key": "IU1011",
          "value": "Muerte del trabajador"
        },
        {
          "key": "IU1012",
          "value": "Muerte del empleador"
        },
        {
          "key": "IU1013",
          "value": "Mutuo consentimiento sin bonificacion"
        },
        {
          "key": "IU1014",
          "value": "Terminación de la obra o labor contratada"
        },
        {
          "key": "IU1015",
          "value": "Abandono de labor"
        },
        {
          "key": "IU1016",
          "value": "Sin información"
        }
      ]
    },
    {
      "key": "L_//_IU9",
      "label": "¿Cual era su tipo de contrato?",
      "required": true,
      "order": 3,
      "controlType": "dropdown",
      "options": [
        {
          "key": "TD1001",
          "value": "Contrato a termino fijo"
        },
        {
          "key": "TD1002",
          "value": "Contrato a termino indefinido"
        },
        {
          "key": "TD1003",
          "value": "Contratos sindical"
        },
        {
          "key": "TD1004",
          "value": "Contratos de Cooperativas y asociados"
        },
        {
          "key": "TD1005",
          "value": "Contrato de aprendizaje"
        },
        {
          "key": "TD1006",
          "value": "Contratos verbales"
        },
        {
          "key": "TD1007",
          "value": "Contrato de obra o labor contratada"
        },
        {
          "key": "TD1008",
          "value": "Prestación de servicios"
        },
        {
          "key": "TD1009",
          "value": "Independiente"
        },
        {
          "key": "TD1010",
          "value": "Libre nombramiento y remoción"
        },
        {
          "key": "TD1011",
          "value": "Fuerzas militares"
        },
        {
          "key": "TD1012",
          "value": "Sin información"
        },
        {
          "key": "TD1013",
          "value": "Sin Empleo"
        }
      ]
    },
    {
      "key": "T_//_prueba",
      "label": "Observaciones del contrato",
      "required": true,
      "order": 4,
      "controlType": "opentext",
      "type": ""
    }
  ];
  //Columnas por dispostivo
  colsMobile = 1; colsTablet = 2; colsPc = 4;

  constructor(public router: Router,private commonService:CommonService) {
    this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
    this.columnsNumHeader = Util.getNumCols(1, 3, 3);
    this.columnsNumCoverageSelected = Util.getNumCols(1, 1, 3);
  }

  ngOnInit() {
    Util.scrollInTop(0);
    this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
  }

  onResize(e) {
    this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
    this.columnsNumHeader = Util.getNumCols(1, 3, 3);
    this.columnsNumCoverageSelected = Util.getNumCols(1, 1, 3);
  }
  selectedCoverage(id) {
    this.coverage_to_process = this.coverages[id];
    this.onResize(event);
    if (this.coverageSelected.index == id && this.coverageSelected.active) {
      this.coverageSelected.active = false;
      this.coverageSelected.index = -1;
    } else {
      this.coverageSelected.active = true;
      this.coverageSelected.index = id;
    }
    if (window.innerWidth < 768) {
      this.coverageIsResponsive = true;
    }
  }
  returnBack() {
    if (window.innerWidth < 768) {
      if (this.coverageSelected.active) {
        this.coverageSelected.active = false;
        this.coverageSelected.index = -1;
        this.coverageIsResponsive = false;
      } else {
        this.router.navigate(['claims/roles']);
      }
    } else {
      this.router.navigate(['claims/roles']);
    }
  }
  noRedirect(){
    event.preventDefault();
    this.commonService.Dialog(1,"Error","Este es un mensaje de error");
  }
}
