import { Component, OnInit,Input } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { Util } from '../../utils/util';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  public myDatePickerOptions: IMyDpOptions;
  @Input() questions:any=[];

  constructor() {
    this.myDatePickerOptions = Util.getLocaleOptions();
   }

  ngOnInit() {
  }

}
