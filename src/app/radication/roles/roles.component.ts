import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Util } from '../../utils/util';
import { IMyDpOptions } from 'mydatepicker';
import { Router } from '@angular/router';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  /**
   * Global Variables de 'ContentComponent'
   */
  public tit_header: string = 'Claims_roles.TitlePage';
  public subHeader = "true";
  public myDatePickerOptions: IMyDpOptions;
  public columnsNum = 1;
  @Input() selectHolder = false;
  @Input() selectBeneficiary = false;
  public Today=new Date();

  constructor(private commonService: CommonService,private router: Router) {
    this.myDatePickerOptions = Util.getLocaleOptions();
    this.Today.setDate(this.Today.getDate()+1);
    this.myDatePickerOptions.disableSince={
      day:this.Today.getDate(),
      month:this.Today.getMonth()+1,
      year:this.Today.getFullYear()};
  }
  ngOnInit() {
    Util.scrollInTop(0);
    this.columnsNum = Util.getNumCols();
    window.onresize = () => {
      this.columnsNum = Util.getNumCols();
    }
  }
  pruLoader() {
    this.commonService.setLoader(true);
  }
  selectRole(role) {
    if (role == 2) {
      this.selectBeneficiary=!this.selectBeneficiary;
    } else if (role == 1) { 
      this.selectHolder=!this.selectHolder;
    }else{
      this.selectHolder=false;
      this.selectBeneficiary=false;
    }
  }
  sendHolder(){
    this.router.navigate(['claims/coverages']);
  }
  sendBeneficiary(){
    this.router.navigate(['claims/coverages']);
  }

}
