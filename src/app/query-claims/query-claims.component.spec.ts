import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryClaimsComponent } from './query-claims.component';

describe('QueryClaimsComponent', () => {
  let component: QueryClaimsComponent;
  let fixture: ComponentFixture<QueryClaimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryClaimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
