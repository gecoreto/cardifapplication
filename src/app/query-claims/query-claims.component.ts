import { Component, OnInit } from '@angular/core';
import { Util } from '../utils/util';

@Component({
  selector: 'app-query-claims',
  templateUrl: './query-claims.component.html',
  styleUrls: ['./query-claims.component.scss']
})
export class QueryClaimsComponent implements OnInit {

  public columnsNum = 1;

  colsMobile = 2; colsTablet = 2; colsPc = 4;

  constructor() { 
    /*this.columnsNum = Util.getNumCols()
    window.onresize = (e) => {
      this.columnsNum = Util.getNumCols();
    };*/
  }

  onResize(e){
    this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
    console.log(this.columnsNum);
  }

  ngOnInit() {
    this.columnsNum = Util.getNumCols(this.colsMobile, this.colsTablet, this.colsPc);
    console.log(this.columnsNum);
  }

}
